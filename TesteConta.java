public class TesteConta {
	public static void main(String [] args){
		Conta conta1 = new Conta();
		Conta contaCorrente1 = new ContaCorrente();
		Conta contaPoupanca1 = new ContaPoupanca();

		conta1.deposita (1000);
		contaCorrente1.deposita(1000);
		contaPoupanca1.deposita(1000);

		conta1.atualiza(0.01);
		contaCorrente1.atualiza(0.01);
		contaPoupanca1.atualiza(0.01);

		System.out.println(conta1.getSaldo());
		System.out.println(contaCorrente1.getSaldo());
		System.out.println(contaPoupanca1.getSaldo());
}
}
